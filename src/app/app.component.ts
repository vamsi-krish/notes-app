import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NotesService } from './services/notes.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit, OnDestroy {
  title = 'notes-app';
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  date = new Date();
  timeStamp: Date;
  timer: any;  
  new_item_form: FormGroup;
  searchText;
  value
  notes = [];
  globalId: any;
  item: any;
  constructor(
    public formBuilder: FormBuilder,
    public noteService: NotesService,
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.getDate();
    this.new_item_form = this.formBuilder.group({
      title: new FormControl('', Validators.required),
    });
    this.notes = this.noteService.getItems();

  }
  
  getDate() {
  this.timer = setInterval(() => {
    this.timeStamp = new Date();
  }, 1000);
  }

  createItem(value){
    this.noteService.createItem(value.title, value.description);
    this.new_item_form.reset();
  }

  loadDate(id, i) {
    const noteTitle = this.noteService.getItemById(id)[0];
    this.new_item_form.controls.title.setValue(noteTitle.title);
    this.globalId = i;
  }

  

  deleteItem() {
    this.notes.splice(this.globalId, 1);
    console.log(this.globalId);
    this.router.navigateByUrl('/');
    this.new_item_form.reset();
  }


  
  ngOnDestroy(): void {
    this.timer.clearinterval();
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
